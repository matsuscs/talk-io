var express = require('express')
var app = express()
var http = require('http').createServer(app);
var io = require('socket.io')(http);//(process.env.PORT || 3011 );
var path = require('path');
var public = path.join(__dirname, 'public');

var Player = require('./Classes/Player.js');
var players = [];
var sockets = [];

var masterSocket = null;

app.use(express.static('public'))

app.get('/', function (req, res) {
    res.sendFile(path.join(public, 'index.html'));
});

io.on('connection', (socket) => {
    console.log('new user connected');

    var _player = new Player();
    var _id = _player.id;

    players[_id] = _player;
    sockets[_id] = socket;

    if (Object.keys(sockets).length == 1) {
        masterSocket = socket;
        console.log('master connected');
    }
    else if(Object.keys(sockets).length > 1)
    {
        io.emit('network', { data: 100});
    }

    socket.on('host', () => {
        if(masterSocket == null)
        {
            masterSocket = socket;
            console.log('master selected');

            socket.emit('host', { host: 1});
        }
        else
        {
            socket.emit('host', { host: 0});
        }
    });

    socket.on('disconnect', () => {
        if (masterSocket != null) {
            if (socket == masterSocket) {
                console.log('master disconnected');
                masterSocket = null;
            }
        }

        for (var i in sockets) {
            if (sockets[i] == socket) {
                delete players[i];
                delete sockets[i];
            }
        }

        if(masterSocket = null && Object.keys(sockets).length > 0)
        {
            for (var i in sockets) {
                masterSocket = sockets[i];
            }
        }

        if (Object.keys(sockets).length <= 1)
        {
            io.emit('network', { data: 101});
        }
        
        console.log(Object.keys(sockets).length);

        console.log('user disconnected');
    });

    socket.on('result', function (data){
        for (var i in sockets) {
            if (sockets[i] != socket) {
                sockets[i].emit('result', { win: 0, type:data.value });
            }
            else
            {
                sockets[i].emit('result', { win: 1, type:data.value });
            }

            players[i].ready = 0;
        }
    });

    socket.on('ready', () => {
        
        var allready = 1;

        for (var i in sockets) {
            if (sockets[i] == socket)
            {
                players[i].ready = 1;
            }

            if (players[i].ready == 0)
            {
                allready = 0;
            }
        }

        if(allready == 1 && Object.keys(sockets).length > 1)
        {
            io.emit('start', { data: 1 });
        }
    });

    socket.on('combo', function (data){
        for (var i in sockets) {
            if (sockets[i] != socket) {
                sockets[i].emit('combo', { combo: data.value, type: data.type});
            }
        }
    });

    socket.on('host', function (data){
        if(socket == masterSocket)
        {
            socket.emit('host', { host: 1 });
        }
        else
        {
            socket.emit('host', { host: 0 });
        }
    });

    socket.on('tilemap', function (data) {
        console.log('tile data map');
        console.log(data.content);

        for (var i in sockets) {
            if (sockets[i] != masterSocket) {
                sockets[i].emit('tilemap', { tilemap: data.content });
            }
        }
    });
});

http.listen(8080, () => {
    console.log('listening on *:8080');
});